import { Component, OnInit } from '@angular/core';
import { ServGralService } from '../../servicios/serv-gral.service';


@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  listaProductos: any;

  constructor(private servicio: ServGralService) {

   }

  ngOnInit() {
    this.verProductos();

  }


  verProductos(){
    return this.servicio.getProductos().subscribe((datos)=> this.listaProductos = datos)
  }


  
    /* Funcion que recibe los datos enviados desde el card producto */
    comprar(id:string, nombP: string, price: number, exist: number, cantSelec: number){
      let idProd: string = id;
      let producto: string = nombP;
      let precio: number = price;
      let existencia: number = exist;
      let cantidad: number = cantSelec;
      let subtotal: number = precio * cantidad;
  
      /* Creando variable objeto para enviar los datos al servicio */
      let datos: any;
  
      datos = {idProd, producto, precio, existencia, cantidad, subtotal};
      console.log("Array del catalogo TS "+ JSON.stringify(datos) );

  // this.servicio.listaDeCarrito.push(datos);

    this.servicio.datosCardProductos(datos);
    }

}
