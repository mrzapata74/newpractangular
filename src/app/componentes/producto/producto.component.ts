import { Component, OnInit } from '@angular/core';
import { ServGralService } from '../../servicios/serv-gral.service';
import { NgForm } from '@angular/forms';//para usar el NgForm de la funcion resetForm()
import { producto } from '../../modelo/producto';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  constructor(private servicio: ServGralService) { }

  ngOnInit() {
  }

  

  onSubmit(productoForm: NgForm){
    this.servicio.agregaProducto(productoForm.value);
    this.resetForm(productoForm);
  }



  resetForm(productoForm?: NgForm){
    if(productoForm != null){
      productoForm.reset();
      this.servicio.prodSeleccionado = new producto//Esta linea deja la variable selectedProducto en blanco
    }
  }


}
