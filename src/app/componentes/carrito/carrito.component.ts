import { Component, OnInit } from '@angular/core';
import { ServGralService } from '../../servicios/serv-gral.service';
import { HttpClient} from '@angular/common/http';
import { producto } from '../../modelo/producto';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  public carrito = [];
  public total: any = [];
  public listaProd: any = [];

 
  constructor(private server: ServGralService, private http: HttpClient, private router: Router) { 

      this.carrito = this.server.listaDeCarrito;
      this.total = this.server.subtotalLista;
      this.listaProd = this.server.todosProd;
      


  }//AQUI FINALIZA EL CONSTRUCTOR


  ngOnInit() {

    // this.verProductos();
    console.log("TODOS LOS PRODUCTOS "+JSON.stringify(this.listaProd))
    console.log("LISTA CARRITO "+JSON.stringify(this.carrito))

  }//AQUI FINALIZA EL NGONINIT


  //FUNCION PAGAR
  pagar(){
    for(let itemProd in this.listaProd){
      for(let itemCarrito of this.carrito){
        if(this.listaProd[itemProd].idProd == itemCarrito.idProd){
          this.listaProd[itemProd].existencia -= itemCarrito.cantidad;
          console.log("Id Producto Todos: "+ this.listaProd[itemProd].idProd);
          console.log("Id Producto Carrito: "+ itemCarrito.idProd);
          console.log("Cantidad que se compra: "+ itemCarrito.cantidad);
          console.log("Cantidad que queda : "+ this.listaProd[itemProd].existencia);
          console.log("Producto Comprado: "+JSON.stringify(this.listaProd[itemProd]));
          console.log("Productos Totales Actualizados: "+JSON.stringify(this.listaProd)); 
          //  this.actualizarCantidad(this.listaProd[itemProd].idProd, this.listaProd );
        }
      }
    }
    this.vaciarCarrito();
    
    let nuevosDatos = this.listaProd;

    firebase.database().ref('/productos/').set(nuevosDatos)
   
    console.log("NUEVOS DATOS "+JSON.stringify(nuevosDatos));
    console.log("VEAMOS EL CARRITO "+JSON.stringify(this.carrito))
    // return this.http.put('https://pruebahttp-41b0a.firebaseio.com/productos/.json', [{"existencia":98,"idProd":35,"precio":5,"producto":"Fresas"},{"existencia":500,"idProd":36,"precio":2,"producto":"Uvas"},{"existencia":1000,"idProd":37,"precio":5,"producto":"Jocotes"},{"existencia":1200,"idProd":41,"precio":20,"producto":"Sandia"}])
    // firebase.database().ref().child('/productos/' + this.listaProd.idProd).set({existencia: this.listaProd.existencia});
    // firebase.database().ref('/productos/').set([{"existencia":0,"idProd":35,"precio":5,"producto":"Fresas"},{"existencia":80,"idProd":36,"precio":2,"producto":"Uvas"},{"existencia":15,"idProd":37,"precio":5,"producto":"Jocotes"}]);
 
  }//AQUI FINALIZA LA FUNCION PAGAR


//FUNCION VACIAR CARRITO
public vaciarCarrito(){
  this.server.listaDeCarrito = [];//Vacia la variable array en el servicio
  this.server.subtotalLista = [];//Vacia la variable array en el servicio
  this.carrito = [];//Vaciar la variable array del TS del carrito
  this.total = 0;//Poner valor cero a variable total
 this.router.navigate(['catalogo']);//Navegar a la vista catalogo

}//AQUI FINALIZA FUNCION VACIAR CARRITO


//Borrar Item del Carrito
borrar(id: number){
   this.carrito.splice(id, 1);

}




}//Aqui finaliza la clase CarritoComponent



