import { TestBed } from '@angular/core/testing';

import { ServGralService } from './serv-gral.service';

describe('ServGralService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServGralService = TestBed.get(ServGralService);
    expect(service).toBeTruthy();
  });
});
