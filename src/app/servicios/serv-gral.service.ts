import { Injectable, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Http, Response } from '@angular/http';
import { producto } from '../modelo/producto';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import * as firebase from 'firebase/app';
import 'firebase/database';


@Injectable({
  providedIn: 'root'
})
export class ServGralService  {

  prodSeleccionado: producto = new producto();

  todosProd: any;

  listaDeCarrito: any[] = [];
  subtotalLista: any[] = [];

  constructor(private http: HttpClient, private http2: Http) {



    var config = {
      apiKey: "AIzaSyAUysLbRejCZmp-WYJ6JuX3nIZdWCGujxc",
      authDomain: "pruebahttp-41b0a.firebaseapp.com",
      databaseURL: "https://pruebahttp-41b0a.firebaseio.com",
      projectId: "pruebahttp-41b0a",
      storageBucket: "pruebahttp-41b0a.appspot.com",
      messagingSenderId: "646340160804"
    };
    firebase.initializeApp(config);
    var db =  firebase.database();

      this.capturarProductos();
    console.log("VARIABLE listaDeCarrito en el Servicio "+this.listaDeCarrito)
  
  }



  //Trayendo Productos de Firebase
  getProductos(){
    return this.http.get('https://pruebahttp-41b0a.firebaseio.com/productos/.json')
  }

  //capturando Productos
  capturarProductos(){
    this.getProductos().subscribe((data)=>this.todosProd = data)
  }

  //Datos que Vienen del Catalogo para agregarse al carrito
  public datosCardProductos(datos: any){
    this.listaDeCarrito.push(datos)
    console.log("Prod Vienen Catalogo "+JSON.stringify(this.listaDeCarrito))
    this.subtotalLista.push(datos.subtotal)
  }




  //Agregando Producto Nuevo a la BD
  agregaProducto(addProducto: producto){
    console.log("DATOS PARA AGREGAR "+JSON.stringify(addProducto))
    this.todosProd.push(addProducto)
    // var datos = [addProducto];

    firebase.database().ref('/productos/').set(this.todosProd);
  }




}//Aqui termina class ServGralService


